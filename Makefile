.POSIX:
.SUFFIXES:
.SUFFIXES: .1 .5 .1.scd .5.scd

VERSION=0.3.0

VPATH=doc
PREFIX?=/usr
_INSTDIR=$(DESTDIR)$(PREFIX)
BINDIR?=$(_INSTDIR)/bin
MANDIR?=$(_INSTDIR)/share/man
GO?=go
GOFLAGS?=

GOSRC!=find . -name '*.go'
GOSRC+=go.mod go.sum

mfn: $(GOSRC)
	$(GO) build $(GOFLAGS) \
		-ldflags "-X main.Version=$(VERSION)" \
		-o $@

DOCS := \
	mfn.1 \
	mfn-config.5

.1.scd.1:
	scdoc < $< > $@

.5.scd.5:
	scdoc < $< > $@

doc: $(DOCS)

all: mfn doc

RM?=rm -f

clean:
	$(RM) $(DOCS) mfn

.DEFAULT_GOAL := all

.PHONY: all mfn clean
