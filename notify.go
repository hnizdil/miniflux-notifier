package main

import (
	"time"

	mf "miniflux.app/client"
)

type Notifier interface {
	Notify(*mf.Entry) error
}

func Test(n Notifier) error {
	e := &mf.Entry{
		ID:         0,
		UserID:     0,
		FeedID:     0,
		Status:     "unread",
		Hash:       "deadbeef",
		Title:      "Test Entry",
		URL:        "https://miniflux.app",
		Date:       time.Now(),
		Content:    "<p>This is a test miniflux entry</p>",
		Author:     "Author",
		Starred:    false,
		Enclosures: make([]*mf.Enclosure, 0),
		Feed:       nil,
	}

	return n.Notify(e)
}
