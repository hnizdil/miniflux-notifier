module git.sr.ht/~gjabell/mfn

go 1.13

require (
	git.sr.ht/~sircmpwn/getopt v0.0.0-20191230200459-23622cc906b3
	github.com/BurntSushi/toml v0.3.1
	github.com/emersion/go-message v0.11.1
	github.com/emersion/go-sasl v0.0.0-20191210011802-430746ea8b9b
	github.com/emersion/go-smtp v0.12.1
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-sqlite3 v2.0.2+incompatible
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	jaytaylor.com/html2text v0.0.0-20200412013138-3577fbdbcff7
	miniflux.app v0.0.0-20200401025025-454eb590cef6
)
