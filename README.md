This is fork of https://git.sr.ht/~gjabell/mfn. Success logs are put into specified file, which is useful, when running with cron. Any other messages are printed to stdout/stderr, which is then sent by e-mail.

# mfn

Send notifications to various sources when [Miniflux](https://miniflux.app) finds new RSS entries.

Currently supported sources include Email, Gotify, and Webhooks.

## Requirements

Dependencies:

- go
- [scdoc](https://git.sr.ht/~sircmpwn/scdoc)

mfn interfaces with an existing Miniflux server. Please see the [Miniflux docs](https://miniflux.app/docs/index.html) for more information.

## Usage

1. Create a TOML configuration file as specified in `man 5 mfn-config`. (Note that only one notifier is required.)

2. Ensure `mfn` can successfully send a notification: `mfn -t [notifier] -c [path to config]` (to view available notifiers, run `mfn -c [path to config] -l`)

3. Run `mfn` once to sync the last entry (no notifications will be sent during the initial population): `mfn -c [path to config]`

4. Periodically run `mfn` (for example, as a cron job) to send notifications when new entries are picked up by Miniflux: `mfn -c [path to config]`

## Implementation

`mfn` uses the official Miniflux Golang client to query Miniflux for new entries. On the first run, `mfn` will determine the id of the most recent entry and store it in a plaintext file. On subsequent runs, `mfn` will only request entries posted after the latest entry, updating the latest entry id on each run. Each entry requested after the initial sync will trigger notifications based on the notifications in the config file.
