package main

import (
	"bytes"
	"crypto/tls"
	"errors"
	"io"
	gomail "net/mail"
	"strings"
	"text/template"
	"time"

	"github.com/emersion/go-message/mail"
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	mf "miniflux.app/client"
)

type Email struct {
	subjectTemplate *template.Template
	bodyTemplate    *template.Template
	emailTo         string
	emailFrom       string
	username        string
	password        string
	server          string
	startTLS        bool
}

func NewEmail(conf *Config) *Email {
	ec := conf.EmailConfig

	return &Email{
		subjectTemplate: ec.SubjectTemplate,
		bodyTemplate:    ec.BodyTemplate,
		emailTo:         ec.EmailTo,
		emailFrom:       ec.EmailFrom,
		username:        ec.Username,
		password:        ec.Password,
		server:          ec.Server,
		startTLS:        ec.StartTLS,
	}
}

func (e *Email) Notify(entry *mf.Entry) error {
	var subject, body bytes.Buffer
	if err := e.subjectTemplate.Execute(&subject, entry); err != nil {
		return err
	}

	if err := e.bodyTemplate.Execute(&body, entry); err != nil {
		return err
	}

	host := e.server
	serverName := e.server
	if !strings.ContainsRune(host, ':') {
		host = host + ":587" // Default to submission port
	} else {
		serverName = host[:strings.IndexRune(host, ':')]
	}

	auth := sasl.NewPlainClient("", e.username, e.password)

	toAddr, err := gomail.ParseAddress(e.emailTo)
	if err != nil {
		return err
	}

	fromAddr, err := gomail.ParseAddress(e.emailFrom)
	if err != nil {
		return err
	}

	conn, err := smtp.Dial(host)
	if err != nil {
		return err
	}
	defer conn.Close()
	if sup, _ := conn.Extension("STARTTLS"); sup {
		if !e.startTLS {
			return errors.New("STARTTLS is supported by this server, " +
				"but is not set in config. " +
				"Add starttls=true under the email configuration.")
		}
		if err = conn.StartTLS(&tls.Config{
			ServerName: serverName,
		}); err != nil {
			return err
		}
	} else {
		if e.startTLS {
			return errors.New("STARTTLS requested, but not supported " +
				"by this SMTP server. Is someone tampering with your " +
				"connection?")
		}
	}
	if err := conn.Auth(auth); err != nil {
		return err
	}

	if err := conn.Mail(fromAddr.Address, nil); err != nil {
		return err
	}
	if err := conn.Rcpt(toAddr.Address); err != nil {
		return err
	}

	wc, err := conn.Data()
	if err != nil {
		return err
	}
	defer wc.Close()

	var header mail.Header
	header.SetAddressList("To", []*mail.Address{(*mail.Address)(toAddr)})
	header.SetAddressList("From", []*mail.Address{(*mail.Address)(fromAddr)})
	header.SetSubject(subject.String())
	header.SetDate(time.Now())
	header.SetContentType("text/plain", map[string]string{"charset": "UTF-8"})

	w, err := mail.CreateSingleInlineWriter(wc, header)
	if err != nil {
		return err
	}
	defer w.Close()

	if _, err := io.Copy(w, &body); err != nil {
		return err
	}

	return nil
}
