package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	"git.sr.ht/~sircmpwn/getopt"
	"jaytaylor.com/html2text"

	mf "miniflux.app/client"
)

var Version string

type Mfn struct {
	conf      *Config
	notifiers map[string]Notifier
}

func loadLastEntry(filepath string) (int64, error) {
	bytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		if os.IsNotExist(err) {
			// no last entry
			return -1, nil
		}
		return -1, err
	}

	str := strings.TrimSpace(string(bytes))
	id, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return -1, err
	}

	return id, nil
}

func saveLastEntry(filepath string, id int64) error {
	return ioutil.WriteFile(filepath, []byte(strconv.FormatInt(id, 10)), 0644)
}

func main() {
	listNotifiers := getopt.Bool("l", false, "list available notifiers")
	showHelp := getopt.Bool("h", false, "show usage")
	showVersion := getopt.Bool("v", false, "show version")

	var testNotifier string
	getopt.StringVar(&testNotifier, "t", "", "test a notifier")

	var configPath string
	getopt.StringVar(&configPath, "c", "", "configuration file path")

	err := getopt.Parse()
	if err != nil {
		log.Fatalf("Failed to parse args: %s", err)
	}

	if *showHelp {
		fmt.Println("Usage: mfn [-v] [-t endpoint] -c config [-l]")
		os.Exit(0)
	}

	if *showVersion {
		fmt.Printf("mfn %s\n", Version)
		os.Exit(0)
	}

	if configPath == "" {
		log.Fatal("You must specify a config location")
	}

	conf, err := LoadConfig(configPath)
	if err != nil {
		log.Fatalf("Failed to load config: %s\n", err)
	}

	notifiers := make(map[string]Notifier)
	if conf.EmailConfig != nil {
		notifiers["email"] = NewEmail(conf)
	}
	if conf.GotifyConfig != nil {
		notifiers["gotify"] = NewGotify(conf)
	}
	if conf.WebhookConfig != nil {
		notifiers["webhook"] = NewWebhook(conf)
	}

	if len(notifiers) == 0 {
		log.Fatalf("You must enable at least one notifier")
	}

	if *listNotifiers {
		list := []string{}
		for n := range notifiers {
			list = append(list, n)
		}
		fmt.Printf("Available notifiers: %s\n", strings.Join(list, ", "))
		os.Exit(0)
	}

	mfn := &Mfn{
		conf:      conf,
		notifiers: notifiers,
	}

	if testNotifier != "" {
		notifier, ok := mfn.notifiers[testNotifier]
		if !ok {
			log.Fatalf("Invalid notifier: %s\n", testNotifier)
		}

		log.Printf("Sending test %s notification...\n", testNotifier)
		if err := Test(notifier); err != nil {
			log.Fatalf("Test notification failed: %s\n", err)
		}
		log.Printf("Test %s notification sent!\n", testNotifier)
		os.Exit(0)
	}

	if err := mfn.execNotifications(); err != nil {
		log.Fatal(err)
	}
}

func (mfn *Mfn) execNotifications() error {
	logFile, err := os.OpenFile(mfn.conf.LogFilePath, os.O_APPEND | os.O_CREATE | os.O_RDWR, 0666)
	if err != nil {
		return fmt.Errorf("Failed to open logfile for writing: %s", mfn.conf.LogFilePath)
	}

	fileLogger := log.New(io.Writer(logFile), "", log.Ldate | log.Ltime)
	client := mf.New(mfn.conf.Server, mfn.conf.Username, mfn.conf.Password)

	categoryId, err := strconv.ParseInt(mfn.conf.CategoryId, 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert category ID to int64: %s", err)
	}

	lastEntry, err := loadLastEntry(mfn.conf.DbPath)
	if err != nil {
		return fmt.Errorf("failed to load last entry: %s", err)
	}

	var notify bool
	filter := &mf.Filter{
		Order: "id",
		CategoryID: categoryId,
	}
	if lastEntry == -1 {
		// this is the first run, since we don't have a last entry
		// in this case, just request the single most recent entry
		// this will be the entry with the largest ID
		filter.Limit = 1
		filter.Direction = "desc"
		// don't send notifications on the first run
		notify = false
	} else {
		// if we already have entries, request all entries since the
		// last entry ID
		filter.Limit = 0
		filter.Direction = "asc"
		filter.AfterEntryID = lastEntry
		filter.Status = "unread"
		notify = true
	}

	newEntriesSet, err := client.Entries(filter)
	if err != nil {
		return fmt.Errorf("failed to fetch entries: %s", err)
	}

	newEntries := newEntriesSet.Entries
	for _, e := range newEntries {
		fileLogger.Printf("New entry: %d\n", e.ID)

		if e.ID > lastEntry {
			lastEntry = e.ID
		}

		if !notify {
			continue
		}

		// always convert content to plaintext
		plainContent, err := html2text.FromString(e.Content,
			html2text.Options{PrettyTables: true})
		if err != nil {
			return err
		}
		e.Content = plainContent

		for name, notifier := range mfn.notifiers {
			if err := notifier.Notify(e); err != nil {
				log.Printf("Failed to send %s notification: %s\n", name, err)
			}
		}
	}
	fileLogger.Printf("Saving new last entry: %d\n", lastEntry)
	if err := saveLastEntry(mfn.conf.DbPath, lastEntry); err != nil {
		return fmt.Errorf("failed to save new entries: %s", err)
	}

	return nil
}
