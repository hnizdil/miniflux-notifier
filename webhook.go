package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"text/template"

	mf "miniflux.app/client"
)

type Webhook struct {
	template *template.Template
	endpoint string
}

func NewWebhook(conf *Config) *Webhook {
	wc := conf.WebhookConfig

	return &Webhook{
		template: wc.Template,
		endpoint: wc.Endpoint,
	}
}

func (w *Webhook) Notify(entry *mf.Entry) error {
	var text bytes.Buffer
	if err := w.template.Execute(&text, entry); err != nil {
		return err
	}

	body := map[string]string{"text": text.String(), "format": "html", "displayName": "Miniflux", "avatarUrl": "https://miniflux.app/favicon.ico"}
	jsonVal, err := json.Marshal(body)
	if err != nil {
		return err
	}

	res, err := http.Post(w.endpoint, "application/json", bytes.NewBuffer(jsonVal))
	if err != nil {
		return err
	}
	resBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil
	}
	log.Printf("Webhook response: %s", string(resBytes))

	return nil
}
