package main

import (
	"bytes"
	"net/http"
	"net/url"
	"strconv"
	"text/template"

	mf "miniflux.app/client"
)

type Gotify struct {
	titleTemplate   *template.Template
	messageTemplate *template.Template
	server          string
	token           string
	priority        int
}

func NewGotify(conf *Config) *Gotify {
	gc := conf.GotifyConfig

	return &Gotify{
		titleTemplate:   gc.TitleTemplate,
		messageTemplate: gc.MessageTemplate,
		server:          gc.Server,
		token:           gc.Token,
		priority:        gc.Priority,
	}
}

func (g *Gotify) Notify(entry *mf.Entry) error {
	var title, message bytes.Buffer
	if err := g.titleTemplate.Execute(&title, entry); err != nil {
		return err
	}

	if err := g.messageTemplate.Execute(&message, entry); err != nil {
		return err
	}

	u, err := url.Parse(g.server)
	if err != nil {
		return err
	}

	q := u.Query()
	q.Set("token", g.token)

	u.Path = u.Path + "message"
	u.RawQuery = q.Encode()

	if _, err := http.PostForm(u.String(), url.Values{
		"title":    {title.String()},
		"message":  {message.String()},
		"priority": {strconv.Itoa(g.priority)},
	}); err != nil {
		return err
	}

	return nil
}
